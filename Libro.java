public class Libro {
  private long isbn;
  private String titulo;
  private String autor;
  private int paginas;

  public void setIsbn (long isbn){
    this.isbn = isbn;
  }
  
  public long getIsbn (){
    return isbn;
  }

  public void setTitulo (String titulo){
    this.titulo = titulo;
  }
  
  public String getTitulo (){
    return titulo;
  }
  public void setAutor (String autor){
    this.autor = autor;
    }
    
  public String getAutor (){
    return autor;
  }

  public void setPaginas (int paginas){
    this.paginas = paginas;
      }

  public int getPaginas (){
    return paginas;  
  }

  public void mostrarInformacion (){
    System.out.println("El libro con ISBN "+isbn+", creado por el autor: "+autor+", tiene "+paginas+" paginas.");
  }
}