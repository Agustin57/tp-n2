public class Principal {
  public static void main(String[] args){
    
    Libro libro1 = new Libro();
    Libro libro2 = new Libro();

    libro1.setTitulo("De animales a dioses");
    libro1.setAutor("Harari Yuval Noah");
    libro1.setIsbn(123658);
    libro1.setPaginas(235);

    libro1.mostrarInformacion();

    libro2.setTitulo("El poder del ahora");
    libro2.setAutor("Eckhart Tolle");
    libro2.setIsbn(169361);
    libro2.setPaginas(135);

    libro2.mostrarInformacion();
  }
}